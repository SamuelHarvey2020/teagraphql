package tea.infra;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TeaEntity {
	
	@Id
	public Long teaId;
	@Column(length=40,nullable=false)
	public String teaName;
	@Column(length=40,nullable=true)
	public String teaCategory;
	@Column(length=140,nullable=true)
	public String teaDescription;
	
	public TeaEntity() {
		
	}
	
	public TeaEntity(Long teaId, String teaName, String teaCategory, String teaDescription) {
		this.teaId = teaId;
		this.teaName = teaName;
		this.teaCategory = teaCategory;
		this.teaDescription = teaDescription;
	}
}
