package tea.domaine;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	private final UserRepository repository;
	
	public UserDetailsServiceImpl(UserRepository repository) {
		this.repository = repository;
	}
	
	@Override
	public UserDetails loadUserByUsername(String emailAddress) throws UsernameNotFoundException {		
		User user = repository.getByEmail(emailAddress);
		
		return new org.springframework.security.core.userdetails.User(user.getEmailAddress(),
						user.getPassword(), getAuthorities(user));		
	}
	
	private Collection<GrantedAuthority> getAuthorities(User user) {
		Collection<GrantedAuthority> authorities = new ArrayList<>();
		if (user.isAdminUser()) {
			authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		} else if (user.isAuthenticatedUser()){
			authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		}
		
		return authorities;
	}

}
